import sys
import os
import time
import codecs
from warnings import warn

from numpy import *
import scipy.constants as co
from scipy.interpolate import interp1d
from scipy.integrate import ode

from matplotlib import pyplot as plt

from chemistry import HumidAir

TEMPERATURE  = 300
PRESSURE     = 1 * co.bar
GAS_DENSITY  = (PRESSURE / (co.k * TEMPERATURE))
Td = 1e-17 * co.centi**2

class Power(object):
    def __init__(self, P0, t0, s):
        self.P0 = P0
        self.t0 = t0
        self.s = s
        
    def __call__(self, t):
        y = t / self.t0
        return self.P0 * exp(-((y - 1) / self.s)**2)


class ReducedField(object):
    def __init__(self, EN0, t0):
        self.EN0 = EN0
        self.t0 = t0

    def __call__(self, t):
        return self.EN0 * sqrt(t / self.t0) * exp(-t / self.t0)

    
def power_function(fname):
    """ Build a function that returns a reduced electric field
    when given e * n * (mu * n) * (e/n)**2 as input.  This is useful
    to find the reduced field corresponding to a power.  """

    en, mun = loadtxt(fname, unpack=True)
    en = r_[0.0, en]
    mun = r_[0.0, mun]  # Ignored

    en_si = en * Td

    w = co.elementary_charge * GAS_DENSITY * mun * en_si**2

    return interp1d(en, w)


def inverse_power_function(fname):
    """ Build a function that returns a reduced electric field
    when given e * n * (mu * n) * (e/n)**2 as input.  This is useful
    to find the reduced field corresponding to a power.  """

    en, mun = loadtxt(fname, unpack=True)
    en = r_[0.0, en]
    mun = r_[0.0, mun]  # Ignored

    en_si = en * Td

    w = co.elementary_charge * GAS_DENSITY * mun * en_si**2

    return interp1d(w, en)


def mean_energy_function(fname):
    en, eng = loadtxt(fname, unpack=True)

    en = r_[0.0, en]
    eng = r_[0.0, eng]

    return interp1d(en, eng)


def power_as_input(rs, n0):
    outpath = os.path.join('output', 'power_as_input')
    os.makedirs(outpath, exist_ok=True)

    inverse_power = inverse_power_function("swarm/mobility.dat")
    power = Power(130 * co.centi**-3, 1e-7, 0.24)

    def dndt(t, n):
        ne = rs.get_species(n, 'e')
        en = inverse_power(abs(power(t) / ne))
        return rs.fderivs(n[:, newaxis], array([en]), TEMPERATURE)

    time = logspace(-11, 0, 10000)
    dt = time[1] - time[0]
    
    r = ode(dndt)
    r.set_integrator('dopri5',
                     nsteps=2000,
                     atol=full_like(n0, 1e-2),
                     rtol=full_like(n0, 1e-4))

    #r.set_integrator('dopri5', nsteps=2000)
    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = squeeze(n0)

    conditions_list = ["E/n (Td)", "T (K)"]
    qtplaskin = os.path.join(outpath, "qtplaskin")
    rs.qtp_init_single(qtplaskin, conditions_list)

    for i, it in enumerate(time[1:]):
        n[:, i + 1] = squeeze(r.integrate(it))
        ne = rs.get_species(n[:, i + 1], 'e')
        pw = power(it)
        en = inverse_power(abs(pw / ne))
        rs.qtp_append_single(it, qtplaskin,
                             where(n[:, i + 1] > 0, n[:, i + 1], 0.0),
                             en, TEMPERATURE)
        
    # Recalculate electric field
    en = inverse_power(power(time) / rs.get_species(n, 'e'))
    mean_energy = mean_energy_function("swarm/mean_energy.dat")

    output(time, en, "reduced_field",
           "Time (s)", "Reduced electric field (Td)", outpath)

    output(time, rs.get_species(n, 'e'), "electron_density",
           "Time (s)", "Electron density (m$^{-3}$)", outpath)

    output(time, power(time), "power_density",
           "Time (s)", "Power density (Wm$^{-3}$)", outpath)
    
    electron_temp = 2 * mean_energy(en) / 3
    output(time, electron_temp, "electron_temperature",
           "Time (s)", "Electron temperature (eV))", outpath)

    output_field_interp(time, en, "swarm/mobility.dat", "mobility",
                        "Time (s)", "Electron mobility Vm$^{2}$s$^{-1}$)",
                        outpath, prefactor=1 / GAS_DENSITY)

    # Species densities
    specpath = os.path.join(outpath, 'species')
    os.makedirs(specpath, exist_ok=True)

    for s in rs.species:
        output(time, rs.get_species(n, s), s,
               "Time (s)", "Density of %s" % s, specpath)    


def field_as_input(rs, n0):
    outpath = os.path.join('output', 'field_as_input')
    os.makedirs(outpath, exist_ok=True)

    reduced_field = ReducedField(330, 1e-7)
    power_per_e = power_function("swarm/mobility.dat")
    
    def dndt(t, n):
        en = reduced_field(t)
        return rs.fderivs(n[:, newaxis], array([en]), TEMPERATURE)

    def jac(t, n):
        en = reduced_field(t)
        return rs.fjacobian(n[:, newaxis], array([en]), TEMPERATURE)[:, :, 0].T
    
    time = logspace(-11, 0, 10000)
    dt = time[1] - time[0]
    
    r = ode(dndt, jac=jac)
    r.set_integrator('vode',
                     method='bdf',
                     # nsteps=2000,
                     # max_step=1e-9,
                     atol=full_like(n0, 1e-8),
                     rtol=full_like(n0, 1e-4)
    )

    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = squeeze(n0)

    conditions_list = ["E/n (Td)", "T (K)"]
    qtplaskin = os.path.join(outpath, "qtplaskin")
    rs.qtp_init_single(qtplaskin, conditions_list)
    
    for i, it in enumerate(time[1:]):
        n[:, i + 1] = squeeze(r.integrate(it))
        rs.qtp_append_single(it, qtplaskin,
                             where(n[:, i + 1] > 0, n[:, i + 1], 0.0),
                             reduced_field(it), TEMPERATURE)

        
    ## OUTPUT
    en = reduced_field(time)
    mean_energy = mean_energy_function("swarm/mean_energy.dat")
    
    
    output(time, en, "reduced_field",
           "Time (s)", "Reduced electric field (Td)", outpath)

    output(time, rs.get_species(n, 'e'), "electron_density",
           "Time (s)", "Electron density (m$^{-3}$)", outpath)

    total_power = power_per_e(en) * rs.get_species(n, 'e')
    output(time, total_power, "power_density",
           "Time (s)", "Power density (Wm$^{-3}$)", outpath)

    electron_temp = 2 * mean_energy(en) / 3
    output(time, electron_temp, "electron_temperature",
           "Time (s)", "Electron temperature (eV))", outpath)

    output_field_interp(time, en, "swarm/mobility.dat", "mobility",
                        "Time (s)", "Electron mobility (Vm$^{2}$s$^{-1}$)",
                        outpath, prefactor=1 / GAS_DENSITY)


    # Species densities
    specpath = os.path.join(outpath, 'species')
    os.makedirs(specpath, exist_ok=True)

    for s in rs.species:
        output(time, rs.get_species(n, s), s,
               "Time (s)", "Density of %s" % s, specpath)
        
    
def output(x, y, basename, xlabel, ylabel, outpath, warn_negative=True):
    with open(os.path.join(outpath, '%s.dat' % basename), "wb") as fout:
        # This is needed because numpy wants a binary file
        def enc_write(s):
            fout.write(s.encode("utf-8"))
            
        enc_write("## Date : %s \n" % time.ctime())
        enc_write("## Command : %s \n" % ' '.join(sys.argv))
        enc_write("## Column 1 : %s \n" % xlabel)
        enc_write("## Column 2 : %s \n" % ylabel)
        enc_write("##\n")

        savetxt(fout, c_[x, y])
        
    plt.clf()
    if warn_negative:
        # Check for negative y
        i0 = argmax(y < 0)
        if y[i0] < 0:
            warn("Negative value at [%s] = %g, [%s] = %g" %
                 (xlabel, x[i0], ylabel, y[i0]))
        
        
    plt.plot(x, where(abs(y) < 1e-20, nan, y))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.loglog()
    plt.grid()
    plt.savefig(os.path.join(outpath, '%s.pdf' % basename))


def output_field_interp(x, en, fname, basename,
                        xlabel, ylabel, outpath, prefactor=1.0):
    """ Outputs as a function of time variable that depends on the reduced
    electric field and is interpolated from values in fname. """

    en1, v = loadtxt(fname, unpack=True)

    en1 = r_[0.0, en1]
    v   = r_[0.0, v]

    f = interp1d(en1, v)
    y = prefactor * f(en)

    output(x, y, basename, xlabel, ylabel, outpath)


    
def main():
    rs = HumidAir(pw=0)
    rs.print_summary()
    
    n0 = rs.zero_densities(1)

    rs.init_neutrals(n0)

    # Set initial densities
    rs.set_species(n0, 'e', 1e5 * co.centi**-3)
    rs.set_species(n0, 'N2+', 0.8 * 1e5 * co.centi**-3)
    rs.set_species(n0, 'O2+', 0.2 * 1e5 * co.centi**-3)

    power_as_input(rs, n0)
    field_as_input(rs, n0)
    

if __name__ == '__main__':
    main()
