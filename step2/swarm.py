"""
This code compares the reaction rates calculated by BOLOS with those obtained
from Bolsig+.  It produces a series of PDF files with plots comparing these 
rates.  To invoke it, use

  python bolsig.py cs.dat bolsig.dat

where 'cs.dat' is a Bolsig+-compatible file of cross sections and bolsig.dat
is a file containing reaction rates as function of the reduced electric field.
We assume that the first column contains electric field whereas each of the 
following columns contain reaction rates, ordered as in the cs.dat file.

As it is currently written, we calculate rates for synthetic air 
(N2:O2 = 80:20) but you can change that below.
"""

import sys
import logging
import argparse
import os
import itertools
import time

import numpy as np
import scipy.constants as co
from bolos import parser, solver, grid


def main():
    argparser = argparse.ArgumentParser()

    argparser.add_argument("input", 
                           help="File with cross-sections in BOLSIG+ format",
                           default="LXCat-June2013.txt")
    argparser.add_argument("--debug", 
                        help="If set, produce a lot of output for debugging", 
                        action='store_true', default=False)
    args = argparser.parse_args()

    if args.debug:
        logging.basicConfig(format='[%(asctime)s] %(module)s.%(funcName)s: '
                            '%(message)s', 
                            datefmt='%a, %d %b %Y %H:%M:%S',
                            level=logging.DEBUG)

    
    # Use a linear grid from 0 to 60 eV with 100 intervals.
    gr = grid.LinearGrid(0, 80., 400)

    # Initiate the solver instance
    bsolver = solver.BoltzmannSolver(gr)

    # Parse the cross-section file in BOSIG+ format and load it into the
    # solver.
    with open(args.input) as fp:
        processes = parser.parse(fp)
    processes = bsolver.load_collisions(processes)
    
    # Set the conditions.  And initialize the solver
    bsolver.target['N2'].density = 0.8
    bsolver.target['O2'].density = 0.2

    # Calculate rates only for N2 and O2
    processes = ['N2 -> N2^+',
                 'O2 -> O2^+',
                 'O2 -> O^-+O',
                 'O2 -> O2^-'
    ]

    # Electric fields
    en = np.logspace(-3, 3, 2500)

    # Create an array to store rates
    bolos = np.empty((len(en), len(processes)))

    # And arrays to store mobility and diffusion coefficient
    mobility = np.empty_like(en)
    diffusion = np.empty_like(en)
    mean_energy = np.empty_like(en)
    
    # Start with Maxwell EEDF as initial guess.  Here we are starting with
    # with an electron temperature of 2 eV
    f0 = bsolver.maxwell(2.0)

    # Solve for each E/n
    for i, ien in enumerate(en):
        print("E/n = %g" % ien)
        bsolver.grid = gr

        logging.info("E/n = %f Td" % ien)
        bsolver.EN = ien * solver.TOWNSEND
        bsolver.kT = 300 * co.k / co.eV
        bsolver.init()

        # Note that for each E/n we start with the previous E/n as
        # a reasonable first guess.
        f1 = bsolver.converge(f0, maxn=200, rtol=1e-4)

        # Second pass: with an automatic grid and a lower tolerance.
        mean_energy1 = bsolver.mean_energy(f1)

        newgrid = grid.QuadraticGrid(0, 10 * mean_energy1, 500)
        
        bsolver.grid = newgrid
        bsolver.init()

        f2 = bsolver.grid.interpolate(f1, gr)
        f2 = bsolver.converge(f2, maxn=400, rtol=1e-6)

        for ip, pname in enumerate(processes):
            bolos[i, ip] = bsolver.rate(f2, bsolver.search(pname))
        
        mobility[i] = bsolver.mobility(f2)
        diffusion[i] = bsolver.diffusion(f2)
        mean_energy[i] = bsolver.mean_energy(f2)

    for ip, pname in enumerate(processes):
        with open("swarm/rate_%.2d.dat" % ip, "w") as fout:
            fout.write("# Process : %s\n" % pname)
            fout.write("# Command : %s\n" % " ".join(sys.argv))
            fout.write("# Date : %s\n" % time.ctime())

            np.savetxt(fout, np.c_[en, bolos[:, ip]])

    with open("swarm/mobility.dat", "w") as fout:
        fout.write("# Process : mobility\n")
        fout.write("# Command : %s\n" % " ".join(sys.argv))
        fout.write("# Date : %s\n" % time.ctime())

        np.savetxt(fout, np.c_[en, mobility])

    with open("swarm/diffusion.dat", "w") as fout:
        fout.write("# Process : diffusion coefficient\n")
        fout.write("# Command : %s\n" % " ".join(sys.argv))
        fout.write("# Date : %s\n" % time.ctime())

        np.savetxt(fout, np.c_[en, diffusion])

    with open("swarm/mean_energy.dat", "w") as fout:
        fout.write("# Process : mean energy\n")
        fout.write("# Command : %s\n" % " ".join(sys.argv))
        fout.write("# Date : %s\n" % time.ctime())

        np.savetxt(fout, np.c_[en, mean_energy])


                
if __name__ == '__main__':
    main()
