"""
  Compare rates obtained from BOLOS with analytical expressions.
"""

import sys
import logging
import argparse
import os
import itertools
import time

import numpy as np
import scipy.constants as co
from matplotlib import pyplot as plt

from bolos import parser, solver, grid

Td = solver.TOWNSEND
EPSILON_1 = 10 * co.eV
EPSILON_2 = 20 * co.eV
C1 = 1e-20
C2 = 1e-21 / co.eV

def main(plot=False):
    logging.basicConfig(format='[%(asctime)s] %(module)s.%(funcName)s: '
                        '%(message)s', 
                        datefmt='%a, %d %b %Y %H:%M:%S',
                        level=logging.DEBUG)

    
    gr = grid.LogGrid(0.0, 100., 1000, s=1e-2)

    # Initiate the solver instance
    bsolver = solver.BoltzmannSolver(gr)

    # Parse the cross-section file in BOSIG+ format and load it into the
    # solver.
    with open('model.bolsigplus') as fp:
        processes = parser.parse(fp)
    processes = bsolver.load_collisions(processes)
    

    # Electric fields
    en = np.linspace(15, 60, 51)
    mean_energy = np.empty_like(en)
    kexc = np.empty_like(en)
    kion = np.empty_like(en)
    

    # Solve for each E/n
    for i, ien in enumerate(en):
        # Set the conditions.  And initialize the solver
        T = temp_analytical(ien)
        print("T = %g eV" % (T / co.eV))
        norm = (1 + np.exp(-EPSILON_1 / T) + np.exp(-EPSILON_1 / T))
        bsolver.target['A'].density = 1.0 / norm
        bsolver.target['A*'].density = np.exp(-EPSILON_1 / T) / norm
        bsolver.target['A+'].density = np.exp(-EPSILON_2 / T) / norm

        f0 = bsolver.maxwell(T / co.eV)

        print("E/n = %g" % ien)
        bsolver.grid = gr

        logging.info("E/n = %f Td" % ien)
        bsolver.EN = ien * solver.TOWNSEND
        bsolver.kT = 300 * co.k / co.eV
        bsolver.init()
        plt.show()
        
        # Note that for each E/n we start with the previous E/n as
        # a reasonable first guess.
        f1 = bsolver.converge(f0, maxn=2000, rtol=1e-7)
        
        mean_energy[i] = bsolver.mean_energy(f1)
        kexc[i] = bsolver.rate(f1, bsolver.search('A -> A*'))
        kion[i] = bsolver.rate(f1, bsolver.search('A -> A+'))

    Te = 2 * mean_energy / 3

    # The analytical rates
    kexc0 = fkexc(Te * co.eV)
    kion0 = fkion(Te * co.eV)

    with open("rates.dat", "w") as fout:
        fout.write("## description : Plasma-chemistry round robin rates from Boltzann solver\n")
        fout.write("## date : %s\n" % time.ctime())
        fout.write("## command : %s\n" % " ".join(sys.argv))
        fout.write("## columns : E/n (Td), T (eV), T_analytical (eV), kexc (m^3s^-1), kexc_analytical (m^3s^1), kion (m^3s^-1), kion_analytical (m^3s^1), rel_error(kexc), rel_error(kion)\n")
        np.savetxt(fout, np.c_[en,
                               Te / co.eV,
                               temp_analytical(en) / co.eV,
                               kexc, kexc0,
                               kion, kion0,
                               abs(kexc - kexc0) / kexc0,
                               abs(kion - kion0) / kion0])

    if plot:
        plt.figure("Electron Temperature")
        plt.plot(en, Te, label="BOLOS")
        plt.plot(en, temp_analytical(en) / co.eV, label="Analytical")
        plt.legend()
        plt.xlabel("E/n (Td)")
        plt.ylabel("Energy (eV)")

        plt.figure("Reaction Rates")
        plt.plot(Te, kexc, label="1 BOLOS")
        plt.plot(Te, kexc0, label="1 Exact")

        plt.plot(Te, kion, label="2 BOLOS")
        plt.plot(Te, kion0, label="1 Exact")

        plt.legend()


        plt.figure("Reaction Rates (error)")
        plt.plot(Te, abs(kexc - kexc0) / kexc0, label="Excitation")
        plt.plot(Te, abs(kion - kion0) / kion0, label="Ionization")
        plt.legend()

        plt.show()

def temp_analytical(en):
    # Analytical expression
    delta = 5e-5
    k0 = 1e-12
    TN = 300
    
    return co.k * TN + (2 / (3 * delta * co.electron_mass) *
                        (co.elementary_charge * en * Td / k0)**2)

    
def fkexc(T):
    return C1 * (np.sqrt(8 / (co.electron_mass * np.pi * T)) * (EPSILON_1 + T) *
                 np.exp(-EPSILON_1 / T))

def fkion(T):
    return C2 * (np.sqrt(8 * T / (co.electron_mass * np.pi))
                 * (EPSILON_2 + 2 * T) * np.exp(-EPSILON_2 / T))

                
if __name__ == '__main__':
    main()
