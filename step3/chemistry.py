from numpy import *
import scipy.constants as co

import chemise as ch

EPSILON_1 = 10 * co.eV
EPSILON_2 = 20 * co.eV
C1 = 1e-20
C2 = 1e-21 / co.eV

class ModelGasAnalytic(ch.ReactionSet):
    
    def __init__(self, pw=0.015, extend=False):
        super(ModelGasAnalytic, self).__init__()
        
        self.add2("e + A -> e + A*",
                  Excitation(C1, EPSILON_1),
                  Deexcitation(C1, EPSILON_1))

        self.add2("e + A -> 2 * e + A+",
                  Ionization(C2, EPSILON_2),
                  Recombination(C2, EPSILON_2))

        self.initialize()

    def exact(self, T):
        n = self.zero_densities(len(T))
        n0 = 1 / (1 + exp(-EPSILON_1 / T) + exp(-EPSILON_2 / T))
        n1 = n0 * exp(-EPSILON_1 / T)
        n2 = n0 * exp(-EPSILON_2 / T)

        self.set_species(n, 'e', n2)
        self.set_species(n, 'A', n0)
        self.set_species(n, 'A*', n1)
        self.set_species(n, 'A+', n2)

        return n

class Excitation(ch.Rate):
    def __init__(self, C, epsilon):
        self.C = C
        self.epsilon = epsilon

    def __call__(self, T, N):
        return (self.C * sqrt(8 / (co.electron_mass * pi * T)) *
                (self.epsilon + T) * exp(-self.epsilon / T))


class Ionization(ch.Rate):
    def __init__(self, C, epsilon):
        self.C = C
        self.epsilon = epsilon

    def __call__(self, T, N):
        return (self.C * sqrt(8 * T / (co.electron_mass * pi)) *
                (self.epsilon + 2 * T) * exp(-self.epsilon / T))


class Deexcitation(ch.Rate):
    def __init__(self, C, epsilon):
        self.epsilon = epsilon
        self.direct = Excitation(C, epsilon)

    def __call__(self, T, N):
        return self.direct(T, N) * exp(self.epsilon / T)


class Recombination(ch.Rate):
    def __init__(self, C, epsilon):
        self.epsilon = epsilon
        self.direct = Ionization(C, epsilon)

    def __call__(self, T, N):
        return self.direct(T, N) * exp(self.epsilon / T) / N

    
