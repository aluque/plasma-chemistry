""" Calculate the analytical cross-sections and write them in the corresponding
file format.
"""
import time
import numpy as np
import scipy.constants as co

ELASTIC = """
ELASTIC
%s
  %g / mass ratio
COMMENT: AUTOMATICALLY GENERATED BY cross_sections.py
UPDATED: %s
"""

EXCITATION = """
EXCITATION
%s -> %s
  %g / threshold energy
COMMENT: AUTOMATICALLY GENERATED BY cross_sections.py
UPDATED: %s
"""

RULE = "-" * 40 + "\n"

epsilon_1 = 10 * co.eV
epsilon_2 = 20 * co.eV
C1 = 1e-20
C2 = 1e-21 / co.eV
delta = 5e-5
k0 = 1e-12

def main():
    eng_eV = np.logspace(-7, 2, 10001)
    eng = eng_eV * co.eV
    v = velocity(eng)

    sigma = k0 / v

    with open("model.bolsigplus", "wb") as fout:
        # ELASTIC
        fout.write((ELASTIC % ('A', delta / 2, time.ctime())).encode("utf-8"))
        fout.write(RULE.encode("utf-8"))
        np.savetxt(fout, np.c_[eng_eV, sigma])
        fout.write(RULE.encode("utf-8"))

        fout.write((ELASTIC % ('A*', delta / 2, time.ctime())).encode("utf-8"))
        fout.write(RULE.encode("utf-8"))
        np.savetxt(fout, np.c_[eng_eV, sigma])
        fout.write(RULE.encode("utf-8"))
        

        # COLLISION #1
        fout.write((EXCITATION % ('A', 'A*', epsilon_1 / co.eV, time.ctime()))
                   .encode("utf-8"))
        fout.write(RULE.encode("utf-8"))
        np.savetxt(fout, np.c_[eng_eV, sigma_1(eng)])
        fout.write(RULE.encode("utf-8"))

        fout.write((EXCITATION % ('A*', 'A', -epsilon_1 / co.eV, time.ctime()))
                   .encode("utf-8"))
        fout.write(RULE.encode("utf-8"))
        np.savetxt(fout, np.c_[eng_eV,
                               ((eng + epsilon_1) / eng
                                * sigma_1(eng + epsilon_1))])
        fout.write(RULE.encode("utf-8"))

        # COLLISION #2
        fout.write((EXCITATION % ('A', 'A+', epsilon_2 / co.eV, time.ctime()))
                   .encode("utf-8"))
        fout.write(RULE.encode("utf-8"))
        np.savetxt(fout, np.c_[eng_eV, sigma_2(eng)])
        fout.write(RULE.encode("utf-8"))

        fout.write((EXCITATION % ('A+', 'A', -epsilon_2 / co.eV, time.ctime()))
                   .encode("utf-8"))
        fout.write(RULE.encode("utf-8"))
        np.savetxt(fout, np.c_[eng_eV,
                               ((eng + epsilon_2) / eng
                                * sigma_2(eng + epsilon_2))])
        fout.write(RULE.encode("utf-8"))
        
        
def velocity(eng):
    return np.sqrt(2 * eng / co.electron_mass)

def sigma_1(eng):
    return np.where(eng < epsilon_1, 0, C1)

def sigma_2(eng):
    return np.where(eng < epsilon_2, 0, C2 * (eng - epsilon_2))


if __name__ == '__main__':
    main()
