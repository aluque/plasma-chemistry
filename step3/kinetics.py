import sys
import os
import time
import codecs
from warnings import warn

from numpy import *
import scipy.constants as co
from scipy.interpolate import interp1d
from scipy.integrate import ode

from matplotlib import pyplot as plt

from chemistry import ModelGasAnalytic

TEMPERATURE  = 300
PRESSURE     = 1 * co.bar
GAS_DENSITY  = (PRESSURE / (co.k * TEMPERATURE))
Td = 1e-17 * co.centi**2

def integrate(rs, n0, Te):
    outpath = os.path.join('output', 'analytic')

    def dndt(t, n):
        n2 = rs.get_species(n, 'A+')
        return rs.fderivs(n[:, newaxis], array([Te]), array([n2]))

    # Estimate the time required for relaxation
    n2 = rs.get_species(n0, 'A+')
    tau = amax(n0 / rs.fderivs(n0, array([Te]), array([n2])))
    print("RELAX TIME = %g" % tau)
    

    time = linspace(0, 100 * tau, 100)
    dt = time[1] - time[0]
    
    r = ode(dndt)
    r.set_integrator('dopri5',
                     rtol=1e-6)

    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = squeeze(n0)

    conditions_list = []
    qtplaskin = os.path.join(outpath, "qtplaskin")
    rs.qtp_init_single(qtplaskin, conditions_list)

    for i, it in enumerate(time[1:]):
        n[:, i + 1] = squeeze(r.integrate(it))
        if not i % 1000 == 0:
            continue

    return n[:, -1]
    
    
def main():
    rs = ModelGasAnalytic()
    rs.print_summary()
    
    n0 = rs.zero_densities(1)
    ne0 = 1 * co.centi**-3
    nstar0 = 1 * co.centi**-3

    # Set initial densities
    rs.set_species(n0, 'e', ne0)
    rs.set_species(n0, 'A*', nstar0)
    rs.set_species(n0, 'A', GAS_DENSITY - ne0 - nstar0)
    rs.set_species(n0, 'A+', ne0)

    T = linspace(0.5 * co.eV, 10 * co.eV, 200)
    nsol = rs.zero_densities(len(T))
    error = rs.zero_densities(len(T))
    nexact = GAS_DENSITY * rs.exact(T)
    
    for i, iT in enumerate(T):
        nsol[:, i] = integrate(rs, n0, iT)
        error[:, i] = (abs(nexact[:, i] - nsol[:, i])
                 / nexact[:, i])
        print("-" * 40)
        print("T = %g eV" % (iT / co.eV))
        print("NUMERICAL:")
        rs.print_species(nsol[:, i, None])

        print("EXACT:")
        rs.print_species(nexact[:, i, None])

        print("RELATIVE ERROR:")
        rs.print_species(error[:, i, None])

    with open("analytical_chemistry.dat", "w") as fout:
        fout.write("## description : Plasma-chemistry round robin chemical system\n")
        fout.write("## date : %s\n" % time.ctime())
        fout.write("## command : %s\n" % " ".join(sys.argv))
        fout.write("## columns : T (eV), [A] (m^-3), [A*] (m^-3), [A+] (m^-3), "
                   "rel_error([A]), rel_error([A*]), rel_error([A+])\n")
        savetxt(fout, c_[T / co.eV,
                         rs.get_species(nsol, 'A'),
                         rs.get_species(nsol, 'A*'),
                         rs.get_species(nsol, 'A+'),
                         rs.get_species(error, 'A'),
                         rs.get_species(error, 'A*'),
                         rs.get_species(error, 'A+')])
        
                      
                      
    for spec in ['A', 'A*', 'A+']:
        plt.plot(T / co.eV, rs.get_species(error, spec), label=spec)

    plt.legend()
    plt.show()
    

if __name__ == '__main__':
    main()
