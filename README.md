# Plasma Chemistry Round Robin

## What is this

This repository contains the contribution of A. Luque to the Plasma Chemistry
Round Robin organized by S. Pancheshnyi.  The code relies on two components

* [BOLOS](https://github.com/aluque/bolos) is used to solve the steady-state
  Boltzmann equation and calculate reaction rates.
* [CHEMISE](https://gitlab.com/aluque/chemise) is used to define the chemical
  system and evaluate derivatives (and, when possible, Jacobians).

To re-run the simulations these two components have to be properly installed
and accesible from the Python path.

## Run the simulations

Each of the two steps contains two main files:

* `swarm.py` calls BOLOS to calculate reaction rates and mobilities.
* `kinetics.py` solves the chemical system.

Therefore in order to repeat the simulations you have to run, in order,
```
  python swarm.py
```
and then
```
  python kinetics.py
```

This will create a folder called `output` containing the simulation results.

## More information

#### Team members
Alejandro Luque, Instituto de Astrofísica de Andalucía (IAA, CSIC).

#### Solvers
* Reaction rates and electron mobilities are calculated from cross-section
  using a two-term Boltzmann solver, based on a reimplementation of the
  method described by G. J. M. Hagelaar and L. C. Pitchford,
  [Plasma Sources Sci. Technol. **14** (2005) 722–733].
* The chemical system is solved with the *Backward-Differentiation Formula*
  method using the `VODE` solver as implemented in SciPy.

#### Chemical system
For `step2` the full chemical system is listed in [`step2/model.pdf`]
(https://gitlab.com/aluque/plasma-chemistry/blob/master/step2/model.pdf).
Note that this set of reactions was designed only to evaluate changes in
the conductivity of air after a discharge within a timescale of about one
microsecond.  It neglects reactions that play a role only after that time.
It also focuses only on the evolution of electrons and a reduced set of
relevant ions.

Note also that the reaction set includes reactions with water vapor.  However,
following the specifications of the round-robin, the water vapor density
is set to zero.

This chemical system was used in the publication
[_Modeling streamer discharges as advancing imperfect conductors_,
 A. Luque, M. González, F. J. Gordillo-Vázquez](https://arxiv.org/abs/1705.03276).

#### Contact
For more information about this code contact A. Luque (aluque@iaa.es).


