import sys
import os
import time
import codecs

from numpy import *
import scipy.constants as co
from scipy.interpolate import interp1d
from scipy.integrate import ode

from matplotlib import pyplot as plt

import chemise as ch

TEMPERATURE  = 300
PRESSURE     = 0.1 * co.bar
GAS_DENSITY  = (PRESSURE / (co.k * TEMPERATURE))
Td = 1e-17 * co.centi**2

class Argon(ch.ReactionSet):
    
    def __init__(self):
        super(Argon, self).__init__()

        self.fix({'Ar': GAS_DENSITY})
        
        self.add("e + Ar -> e + Ar*",
                 ch.LogLogInterpolate0("swarm/rate_00.dat"))

        self.add("e + Ar -> e + e + Ar+",
                 ch.LogLogInterpolate0("swarm/rate_01.dat"))

        self.add("Ar+ + e + e -> Ar + e",
                 ETemperaturePower(8.75e-27 * co.centi**6,
                                   9. / 2.,
                                   1,
                                   "swarm/mean_energy.dat"))
        
        self.initialize()


class ETemperaturePower(ch.LogLogInterpolate0):
    def __init__(self, k0, power, T0, *args, **kwargs):
        self.k0 = k0
        self.power = power
        self.T0 = T0

        super(ETemperaturePower, self).__init__(*args, **kwargs)
        

    def __call__(self, EN):
        Te = 2 * exp(self.s(log(EN))) / 3
        return full_like(EN, self.k0 * (self.T0 / Te)**self.power)

    def latex(self):
        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"
                % (self.k0, self.T0, self.power))


class Power(object):
    def __init__(self, P0, t0):
        self.P0 = P0
        self.t0 = t0

    def __call__(self, t):
        return self.P0 * (t / self.t0)**2 * exp(-t / self.t0)


class ReducedField(object):
    def __init__(self, EN0, t0):
        self.EN0 = EN0
        self.t0 = t0

    def __call__(self, t):
        return self.EN0 * sqrt(t / self.t0) * exp(-t / self.t0)

    
def power_function(fname):
    """ Build a function that returns a reduced electric field
    when given e * n * (mu * n) * (e/n)**2 as input.  This is useful
    to find the reduced field corresponding to a power.  """

    en, mun = loadtxt(fname, unpack=True)
    en = r_[0.0, en]
    mun = r_[0.0, mun]  # Ignored

    en_si = en * Td

    w = co.elementary_charge * GAS_DENSITY * mun * en_si**2

    return interp1d(en, w)


def inverse_power_function(fname):
    """ Build a function that returns a reduced electric field
    when given e * n * (mu * n) * (e/n)**2 as input.  This is useful
    to find the reduced field corresponding to a power.  """

    en, mun = loadtxt(fname, unpack=True)
    en = r_[0.0, en]
    mun = r_[0.0, mun]  # Ignored

    en_si = en * Td

    w = co.elementary_charge * GAS_DENSITY * mun * en_si**2

    return interp1d(w, en)


def mean_energy_function(fname):
    en, eng = loadtxt(fname, unpack=True)

    en = r_[0.0, en]
    eng = r_[0.0, eng]

    return interp1d(en, eng)


def power_as_input(rs, n0):
    inverse_power = inverse_power_function("swarm/mobility.dat")
    power = Power(10 * co.centi**-3, 1 * co.milli)

    def dndt(t, n):
        ne = rs.get_species(n, 'e')
        en = inverse_power(power(t) / ne)
        return rs.fderivs(n[:, newaxis], array([en]))
    
    time = logspace(-10, -2, 100000)
    dt = time[1] - time[0]
    
    r = ode(dndt)
    r.set_integrator('vode', method='bdf', nsteps=2000,
                     atol=full_like(n0, 1e-3),
                     rtol=full_like(n0, 1e-8))

    #r.set_integrator('dopri5', nsteps=2000)
    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = squeeze(n0)

    for i, it in enumerate(time[1:]):
        n[:, i + 1] = squeeze(r.integrate(it))
        
    # Recalculate electric field
    en = inverse_power(power(time) / rs.get_species(n, 'e'))
    mean_energy = mean_energy_function("swarm/mean_energy.dat")

    outpath = os.path.join('output', 'power_as_input')
    os.makedirs(outpath, exist_ok=True)

    output(time, en, "reduced_field",
           "Time (s)", "Reduced electric field (Td)", outpath)

    output(time, rs.get_species(n, 'e'), "electron_density",
           "Time (s)", "Electron density (m$^{-3}$)", outpath)

    output(time, rs.get_species(n, 'Ar*'), "Ar_star_density",
           "Time (s)", "Ar* density (m$^{-3}$)", outpath)

    output(time, power(time), "power_density",
           "Time (s)", "Power density (Wm$^{-3}$)", outpath)
    
    electron_temp = 2 * mean_energy(en) / 3
    output(time, electron_temp, "electron_temperature",
           "Time (s)", "Electron temperature (eV))", outpath)

    output_field_interp(time, en, "swarm/mobility.dat", "mobility",
                        "Time (s)", "Electron mobility Vm$^{2}$s$^{-1}$)",
                        outpath, prefactor=1 / GAS_DENSITY)

    output_field_interp(time, en, "swarm/rate_00.dat", "excitation",
                        "Time (s)",
                        " Rate of 'e + Ar -> e + Ar*' (m$^{3}$s$^{-1}$)",
                        outpath)

    output_field_interp(time, en, "swarm/rate_01.dat", "ionization",
                        "Time (s)",
                        " Rate of 'e + Ar -> e + e + Ar+' (m$^{3}$s$^{-1}$)",
                        outpath)
    
    


def field_as_input(rs, n0):
    reduced_field = ReducedField(43, 1 * co.milli)
    power_per_e = power_function("swarm/mobility.dat")
    
    def dndt(t, n):
        en = reduced_field(t)
        return rs.fderivs(n[:, newaxis], array([en]))
    
    time = logspace(-6, -2, 100000)
    dt = time[1] - time[0]
    
    r = ode(dndt)
    r.set_integrator('vode', method='bdf', nsteps=2000,
                     atol=full_like(n0, 1e-4),
                     rtol=full_like(n0, 1e-9))

    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = squeeze(n0)

    for i, it in enumerate(time[1:]):
        n[:, i + 1] = squeeze(r.integrate(it))
        
    ## OUTPUT
    en = reduced_field(time)
    mean_energy = mean_energy_function("swarm/mean_energy.dat")
    
    outpath = os.path.join('output', 'field_as_input')
    os.makedirs(outpath, exist_ok=True)
    
    output(time, en, "reduced_field",
           "Time (s)", "Reduced electric field (Td)", outpath)

    output(time, rs.get_species(n, 'e'), "electron_density",
           "Time (s)", "Electron density (m$^{-3}$)", outpath)

    output(time, rs.get_species(n, 'Ar*'), "Ar_star_density",
           "Time (s)", "Ar* density (m$^{-3}$)", outpath)

    total_power = power_per_e(en) * rs.get_species(n, 'e')
    output(time, total_power, "power_density",
           "Time (s)", "Power density (Wm$^{-3}$)", outpath)

    electron_temp = 2 * mean_energy(en) / 3
    output(time, electron_temp, "electron_temperature",
           "Time (s)", "Electron temperature (eV))", outpath)

    output_field_interp(time, en, "swarm/mobility.dat", "mobility",
                        "Time (s)", "Electron mobility (Vm$^{2}$s$^{-1}$)",
                        outpath, prefactor=1 / GAS_DENSITY)

    output_field_interp(time, en, "swarm/rate_00.dat", "excitation",
                        "Time (s)",
                        " Rate of 'e + Ar -> e + Ar*' (m$^{3}$s$^{-1}$)",
                        outpath)

    output_field_interp(time, en, "swarm/rate_01.dat", "ionization",
                        "Time (s)",
                        " Rate of 'e + Ar -> e + e + Ar+' (m$^{3}$s$^{-1}$)",
                        outpath)
    
    
def output(x, y, basename, xlabel, ylabel, outpath):
    with open(os.path.join(outpath, '%s.dat' % basename), "wb") as fout:
        # This is needed because numpy wants a binary file
        def enc_write(s):
            fout.write(s.encode("utf-8"))
            
        enc_write("## Date : %s \n" % time.ctime())
        enc_write("## Command : %s \n" % ' '.join(sys.argv))
        enc_write("## Column 1 : %s \n" % xlabel)
        enc_write("## Column 2 : %s \n" % ylabel)
        enc_write("##\n")

        savetxt(fout, c_[x, y])
        
    plt.clf()
    plt.plot(x, y)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.loglog()
    plt.grid()
    plt.savefig(os.path.join(outpath, '%s.pdf' % basename))


def output_field_interp(x, en, fname, basename,
                        xlabel, ylabel, outpath, prefactor=1.0):
    """ Outputs as a function of time variable that depends on the reduced
    electric field and is interpolated from values in fname. """

    en1, v = loadtxt(fname, unpack=True)

    en1 = r_[0.0, en1]
    v   = r_[0.0, v]

    f = interp1d(en1, v)
    y = prefactor * f(en)

    output(x, y, basename, xlabel, ylabel, outpath)


    
def main():
    rs = Argon()
    rs.print_summary()

    n0 = rs.zero_densities(1)

    # Set initial densities
    rs.set_species(n0, 'e', 1 * co.centi**-3)
    rs.set_species(n0, 'Ar+', 1 * co.centi**-3)
    
    power_as_input(rs, n0)
    field_as_input(rs, n0)
    

if __name__ == '__main__':
    main()
