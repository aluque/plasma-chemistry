import sys
import logging
import argparse
import os
import itertools
import time

import numpy as np
import scipy.constants as co
from bolos import parser, solver, grid


def main():
    logging.basicConfig(format='[%(asctime)s] %(module)s.%(funcName)s: '
                        '%(message)s', 
                        datefmt='%a, %d %b %Y %H:%M:%S',
                        level=logging.DEBUG)

    
    # Use a linear grid from 0 to 60 eV with 100 intervals.
    gr = grid.LinearGrid(0, 80., 400)

    # Initiate the solver instance
    bsolver = solver.BoltzmannSolver(gr)

    # Parse the cross-section file in BOSIG+ format and load it into the
    # solver.
    fname = "cross_sections.bolsigplus"
    
    with open(fname) as fp:
        processes = parser.parse(fp)
    processes = bsolver.load_collisions(processes)
    
    # Set the conditions.  And initialize the solver
    bsolver.target['Ar'].density = 1.0

    processes = ['Ar -> Ar^+',
                 'Ar -> Ar*']

    # Electric fields
    en = np.r_[0:20.1:0.1]

    # Create an array to store rates
    bolos = np.empty((len(en), len(processes)))

    # And arrays to store mobility and diffusion coefficient
    mobility = np.empty_like(en)
    diffusion = np.empty_like(en)
    mean_energy = np.empty_like(en)
    
    # Start with Maxwell EEDF as initial guess.  Here we are starting with
    # with an electron temperature of 2 eV
    f0 = bsolver.maxwell(2.0)

    # Solve for each E/n
    for i, ien in enumerate(en):
        print("E/n = %g" % ien)
        bsolver.grid = gr

        logging.info("E/n = %f Td" % ien)
        bsolver.EN = ien * solver.TOWNSEND
        bsolver.kT = 300 * co.k / co.eV
        bsolver.init()

        # Note that for each E/n we start with the previous E/n as
        # a reasonable first guess.
        f1 = bsolver.converge(f0, maxn=200, rtol=1e-5)

        # Second pass: with an automatic grid and a lower tolerance.
        mean_energy1 = bsolver.mean_energy(f1)

        newgrid = grid.QuadraticGrid(0, 15 * mean_energy1, 4000)
        
        bsolver.grid = newgrid
        bsolver.init()

        f2 = bsolver.grid.interpolate(f1, gr)
        f2 = bsolver.converge(f2, maxn=400, rtol=1e-6)

        for ip, pname in enumerate(processes):
            bolos[i, ip] = bsolver.rate(f2, bsolver.search(pname))
        
        mobility[i] = bsolver.mobility(f2)
        diffusion[i] = bsolver.diffusion(f2)
        mean_energy[i] = bsolver.mean_energy(f2)

    np.savetxt("task3.dat",
               np.c_[en,
                     bolos[:, 0],
                    bolos[:, 1],
                     mobility])


                
if __name__ == '__main__':
    main()
