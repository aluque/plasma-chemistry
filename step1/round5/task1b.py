import sys
import os
import time
import codecs

from numpy import *
import scipy.constants as co
from scipy.interpolate import interp1d
from scipy.integrate import ode

from matplotlib import pyplot as plt

import chemise as ch

TEMPERATURE  = 300
PRESSURE     = 0.1 * co.bar
GAS_DENSITY  = (PRESSURE / (co.k * TEMPERATURE))
Td = 1e-17 * co.centi**2

class Argon(ch.ReactionSet):
    
    def __init__(self):
        super(Argon, self).__init__()

        self.add("e + Ar -> e + e + Ar+",
                 KFunc(1e-16 * co.centi**3, 1e-1))

        self.add("Ar+ + e + e -> Ar + e",
                 ch.Constant(1e-25 * co.centi**6))

        self.add("e + Ar -> e + Ar*",
                 KFunc(1e-13 * co.centi**3, 1e-1))

        self.add("Ar* -> Ar",
                 ch.Constant(1e6))
        
        self.initialize()


class KFunc(object):
    def __init__(self, k0, t0):
        self.k0 = k0
        self.t0 = t0

    def __call__(self, t):
        return self.k0 * (t / self.t0)**2 * exp(-t / self.t0)

    
def main():
    rs = Argon()
    rs.print_summary()

    n0 = rs.zero_densities(1)

    # Set initial densities
    rs.set_species(n0, 'e', 1 * co.centi**-3)
    rs.set_species(n0, 'Ar+', 1 * co.centi**-3)
    rs.set_species(n0, 'Ar', 1e19 * co.centi**-3)
    
    def dndt(t, n):
        return rs.fderivs(n[:, newaxis], array([t]))
    
    time = logspace(-7, 0, 10000)
    
    r = ode(dndt)
    r.set_integrator('vode', method='bdf', nsteps=10000,
                     max_step=1e-6,
                     atol=full_like(n0, 1e-5),
                     rtol=full_like(n0, 1e-10))

    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = squeeze(n0)

    for i, it in enumerate(time[1:]):
        n[:, i + 1] = squeeze(r.integrate(it))
    
    savetxt("task1b.dat", c_[time,
                             rs.get_species(n, 'e') / co.centi**-3,
                             rs.get_species(n, 'Ar*') / co.centi**-3,
                             rs.get_species(n, 'Ar') / co.centi**-3])
    
    plt.plot(time, rs.get_species(n, 'e'))
    plt.plot(time, rs.get_species(n, 'Ar*'))
    plt.plot(time, rs.get_species(n, 'Ar'))
    plt.show()

    
if __name__ == '__main__':
    main()
